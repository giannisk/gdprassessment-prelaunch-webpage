# GDPR Compliance Assessment - Pre-Launch Webpage Design

Pre-launch webpage for the GDPR Compliance Assessment, an open-source web-based system that helps evaluate your organisation's alignment with the EU GDPR.

![Webpage Preview](/img/preview.png)

Based on the [Coming Soon](https://github.com/BlackrockDigital/startbootstrap-coming-soon) Bootstrap template.

## License

Final work is made available under the [MIT License](LICENSE).

Coming Soon is available under the MIT License.
